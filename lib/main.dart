import 'package:flutter/material.dart';
import 'package:english_words/english_words.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      title: 'Welcome to Flutter',
      theme: ThemeData(
        primaryColor: Colors.brown,
      ),
      // home: RandomWords(),
      home: Scaffold(
        appBar: AppBar(
          title: Text('appTitle'),
        ),
        body: CustomForm(),
      ),
    );
  }
}

// Define a Custom Form Widget
class CustomForm extends StatefulWidget {
  @override
  CustomFormState createState() => CustomFormState();
}


// Define a corresponding State class. This class will hold the data related to
// the form.
class CustomFormState extends State<CustomForm> {
  // Create a global key that will uniquely identify the Form widget and allow
  // us to validate the form
  //
  // Note: This is a `GlobalKey<FormState>`, not a GlobalKey<MyCustomFormState>!
  final _formKey = GlobalKey<FormState>();

  String _formVal;

  @override
  Widget build(BuildContext context) => Form(
    key: _formKey,
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        TextFormField(
          // The validator receives the text the user has typed in
          validator: (value) {
            if (value.isEmpty) {
              return 'Please enter some text';
            } else {
              _formVal = value;
            }
          },
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 16.0),
          child: RaisedButton(
            onPressed: () {
              // Validate will return true if the form is valid, or false if
              // the form is invalid.
              if (_formKey.currentState.validate()) {
                // If the form is valid, we want to show a Snackbar
                Scaffold.of(context)
                    .showSnackBar(SnackBar(content: Text(_formVal)));
              }
            },
            child: Text('Submit'),
          ),
        ),
      ]
    ),
  );

  

} // CustomFormState




















class RandomWordsState extends State<RandomWords> {
  final List<WordPair> _suggestions = <WordPair>[];
  final TextStyle _biggerFont = const TextStyle(fontSize: 18.0);
  final Set<WordPair> _saved = Set<WordPair>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Startup Name Generator'),
        actions: <Widget>[
            IconButton(
              icon: Icon(Icons.list), 
              onPressed: _pushSaved,
              ),
          ],
      ),
      body: _buildSuggestions(),
    );
  }

  /// * /*1*/ The [itemBuilder] callback is called once per suggested word pairing, and places each suggestion into a [ListTile] row. 
  ///   For even rows, the function adds a [ListTile] row for the word pairing. 
  ///   For odd rows, the function adds a [Divider] widget to visually separate the entries. Note that the divider may be difficult to see on smaller devices.
  /// * /*2*/ Add a one-pixel-high divider widget before each row in the [ListView].
  /// * /*3*/ The expression `i ~/ 2` divides `i` by `2` and returns an integer result. For example: `1, 2, 3, 4, 5` becomes `0, 1, 1, 2, 2`. 
  ///   This calculates the actual number of word pairings in the [ListView], minus the divider widgets.
  /// * /*4*/ If you’ve reached the end of the available word pairings, then generate `10` more and add them to the suggestions list.
  /// 
  /// The [_buildSuggestions()] function calls [_buildRow()] once per word pair. This function displays each new pair in a [ListTile], 
  ///   which allows you to make the rows more attractive in the next step.
  Widget _buildSuggestions() {
    return ListView.builder(
      padding: const EdgeInsets.all(16.0),
      itemBuilder: /*1*/ (context, i) {
        if (i.isOdd) return Divider(); /*2*/

        final index = i ~/ 2; /*3*/
        if (index >= _suggestions.length) {
          _suggestions.addAll(generateWordPairs().take(10)); /*4*/
        }
        return _buildRow(_suggestions[index]);
      }
    );
  }

  Widget _buildRow (WordPair pair) {
    final bool alreadySaved = _saved.contains(pair);
    return ListTile(
      title: Text(
        pair.asPascalCase,
        style: _biggerFont,
      ),
      trailing: Icon(
        alreadySaved ? Icons.favorite : Icons.favorite_border,
        color: alreadySaved ? Colors.pinkAccent : null,
      ),
      onTap: () {
        setState(() {
         if (alreadySaved) {
           _saved.remove(pair);
         } else {
           _saved.add(pair);
         }
        });
      },
    );
  }


  void _pushSaved() {
    Navigator.of(context).push(
      MaterialPageRoute<void>(
        builder: (BuildContext context) {
          final Iterable<ListTile> tiles = _saved.map(
            (WordPair pair) {
              return ListTile(
                title: Text(
                  pair.asPascalCase,
                  style: _biggerFont,
                ),
              );
            }
          );
          final List<Widget> divided = ListTile
            .divideTiles(
              context: context,
              tiles: tiles,
            )
            .toList();

            return Scaffold(
              appBar: AppBar(
                title: Text('Saved Suggestions'),
              ),
              body: ListView(children: divided),
            );
        }
      ),
    );
  }
}

class RandomWords extends StatefulWidget {
  @override
  RandomWordsState createState() => RandomWordsState();
}